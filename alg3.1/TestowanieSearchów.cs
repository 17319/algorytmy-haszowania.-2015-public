﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace alg3._
{

	partial class Program
	{
		static public void TestowanieSearchów()
		{
			// sprawdzamy search liniowe
			indeksLiniowy=true;
			sw.Start();
			for (int i = 0; i < wielkoscProbyDoFora/100; i++)
			{
				Search(generator(dlugoscStringa), Tpoj);
			}
			sw.Stop();
			Console.WriteLine("\nCzas search liniowego: \t\t" + sw.ElapsedMilliseconds);
			swritter.WriteLine("\nCzas search liniowego: \t\t" + sw.ElapsedMilliseconds);
			foreach(var w in T)
			{
				int i=0;
				Tpoj[i]=T[i++];
			}
			
			sw.Reset();
			
			
			// sprawdzamy search podwojnego
			indeksLiniowy=false;
			T = new String[rozmiarTablicy];
			indeksLiniowy = false;
			sw.Start();
			for (int i = 0; i < wielkoscProbyDoFora/100; i++)
			{
				Search(generator(dlugoscStringa), Tpodw);
			}
			sw.Stop();
			Console.WriteLine("Czas search podwojnego: \t" + sw.ElapsedMilliseconds);
			swritter.WriteLine("Czas search podwojnego: \t" + sw.ElapsedMilliseconds);
			foreach(var w in T)
			{
				int i=0;
				Tpodw[i]=T[i++];
			}
			sw.Reset();
			
			// sprawdzamy search słownika
			sw.Start();
			for (int i = 0; i < wielkoscProbyDoFora/100; i++)
			{
				slownik.ContainsValue(generator(dlugoscStringa));
			}
			sw.Stop();
			Console.WriteLine("Czas search słownika: \t\t" + sw.ElapsedMilliseconds);
			swritter.WriteLine("Czas search słownika: \t\t" + sw.ElapsedMilliseconds);
			sw.Reset();
		}
	}
}
