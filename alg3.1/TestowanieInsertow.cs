﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace alg3._
{

	partial class Program
	{
		static public void TestowanieInsertow()
		{
			// sprawdzamy inserta liniowe
			
			sw.Start();
			for (int i = 0; i < wielkoscProbyDoFora; i++)
			{
				Insert(generator(dlugoscStringa));
			}
			sw.Stop();
			Console.WriteLine("Czas inserta liniowego: \t" + sw.ElapsedMilliseconds);
			swritter.WriteLine("Czas inserta liniowego: \t" + sw.ElapsedMilliseconds);
			foreach(var w in T)
			{
				int i=0;
				Tpoj[i]=T[i++];
			}
			
			sw.Reset();
			
			
			// sprawdzamy inserta podwojnego
			T = new String[rozmiarTablicy];
			indeksLiniowy = false;
			sw.Start();
			for (int i = 0; i < wielkoscProbyDoFora; i++)
			{
				Insert(generator(dlugoscStringa));
			}
			sw.Stop();
			Console.WriteLine("Czas inserta podwojnego: \t" + sw.ElapsedMilliseconds);
			swritter.WriteLine("Czas inserta podwojnego: \t" + sw.ElapsedMilliseconds);
			foreach(var w in T)
			{
				int i=0;
				Tpodw[i]=T[i++];
			}
			sw.Reset();
			
			// sprawdzamy inserta słownika
			sw.Start();
			for (int i = 0; i < wielkoscProbyDoFora; i++)
			{
				slownik.Add(i,generator(dlugoscStringa));
			}
			sw.Stop();
			Console.WriteLine("Czas inserta słownika: \t\t" + sw.ElapsedMilliseconds);
			swritter.WriteLine("Czas inserta słownika: \t\t" + sw.ElapsedMilliseconds);
			sw.Reset();
			
		}
	}
}
