﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.IO;
namespace alg3._
{
	partial class Program
	{
		static string [] T;
		static string [] Tpoj;
		static string [] Tpodw;
		static bool indeksLiniowy = true;
		static int rozmiarTablicy = Int32.MaxValue/10000;
		static int dlugoscStringa = 10;
		static int wielkoscProbyDoFora = 10000;
		static Stopwatch sw = new Stopwatch();
		static Dictionary<int,string> slownik;
		static StreamWriter swritter = new StreamWriter("wyniki.txt",false);
		
		public static void Main(string[] args)
		{
			
		poczatek:
			T = new String[rozmiarTablicy];
			Tpoj = new String[rozmiarTablicy];
			Tpodw = new String[rozmiarTablicy];
			slownik = new Dictionary<int, string>(rozmiarTablicy);
			
			Console.WriteLine("\n\nPoniżej są wyniki dla stringa o długości: " + dlugoscStringa);
			swritter.WriteLine("\n\nPoniżej są wyniki dla stringa o długości: " + dlugoscStringa);
			TestowanieInsertow();
			TestowanieSearchów();
			TestowanieRemovow();
			
			dlugoscStringa *=10;
			if (dlugoscStringa<=10000)
				goto poczatek;
			
			swritter.Close();
			
			
			Console.WriteLine("Koniec");
			Console.ReadKey();
		}
		
		
		
		
	}
}