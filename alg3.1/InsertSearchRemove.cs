﻿
using System;

namespace alg3._
{


	public partial class Program
	{
		
		public static void Insert(string str)
		{
			
			int indeks = Indeks(str);
			int poczatek = indeks;
			if(T[indeks] == null)
				T[indeks] = str;
			else //iteruj aż znajdziesz wolne miejsce, chyba że wszystkie miejsca są zajęte
			{
				while(T[indeks] != null)
				{
					indeks++;
					if (indeks==T.Length) indeks=0;
					if (indeks==poczatek) throw new Exception("Tablica pełna");
				}
				T[indeks] = str;
			}
		}
		public static int Search(string str, string[] Tablica)
		{
			int indeks = Indeks(str);
			int poczatek = indeks;
			if (Tablica[indeks]==str)
			{
				return indeks;
			}
			else
			{
				while(!(Tablica[indeks] == str))
				{
					indeks++;
					if (indeks==Tablica.Length) indeks=0;
					if (indeks==poczatek) return -1;
				}
				return indeks;
			}
			
		}
		public static void Remove(string str, string[] Tablica)
		{
			int indeks = Indeks(str);
			int poczatek = indeks;
			if (Tablica[indeks]==str)
			{
				Tablica[indeks]=null;
			}
			else
			{
				while(Tablica[indeks] != str)
				{
					indeks++;
					if (indeks==Tablica.Length) indeks=0;
					if (indeks==poczatek) //throw new Exception("Nie usunieto");
						break;
				}
				T[indeks]=null;
			}
		}
		
		
		
		
		
		
		
	}
}
