﻿
using System;

namespace alg3._
{
	partial class Program
	{
		public static int Indeks(string str)
		{
			if(indeksLiniowy)
				return IndeksLiniowy(str);
			else
				return IndeksPodwojny(str);
		}
		public static int IndeksLiniowy(string str)//http://smurf.mimuw.edu.pl/node/334
		{
			return (funkcjaHaszujaca1(str) + 11) % T.Length;
		}
		public static int IndeksPodwojny(string str)
		{
			return (funkcjaHaszujaca1(str) + funkcjaHaszujaca2(str) * 11) % T.Length;
		}
		public static int funkcjaHaszujaca1(string k)
		{
			int r = 0;
			foreach(char c in k)
				r += Convert.ToInt32(c);
			return r;
		}
		public static int funkcjaHaszujaca2(string k)
		{
			int r = 0;
			foreach(char c in k)
			{
				if(Convert.ToInt32(c)%3==1)
					r -= Convert.ToInt32(c);
				else
					r += Convert.ToInt32(c);
			}
			return Math.Abs(r);
		}
		
		
		
		static public String generator(int a)
		{
			Random random = new Random();
			var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
			var stringChars = new char[a];

			for (int i = 0; i < stringChars.Length; i++)
			{
				stringChars[i] = chars[random.Next(chars.Length)];
			}

			var finalString = new String(stringChars);
			return finalString;
		}
		
		
		
	}
}
