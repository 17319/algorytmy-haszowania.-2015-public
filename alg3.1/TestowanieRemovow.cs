﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace alg3._
{

	partial class Program
	{
		static public void TestowanieRemovow()
		{
			// sprawdzamy remove liniowe
			indeksLiniowy=true;
			sw.Start();
			for (int i = 0; i < wielkoscProbyDoFora/100; i++)
			{
				Remove(generator(dlugoscStringa),Tpoj);
			}
			sw.Stop();
			Console.WriteLine("\nCzas remove liniowego: \t\t" + sw.ElapsedMilliseconds);
			swritter.WriteLine("\nCzas remove liniowego: \t\t" + sw.ElapsedMilliseconds);
			foreach(var w in T)
			{
				int i=0;
				Tpoj[i]=T[i++];
			}
			
			sw.Reset();
			
			
			// sprawdzamy remove podwojnego
			indeksLiniowy = false;
			sw.Start();
			for (int i = 0; i < wielkoscProbyDoFora/100; i++)
			{
				Remove(generator(dlugoscStringa),Tpodw);
			}
			sw.Stop();
			Console.WriteLine("Czas remove podwojnego: \t" + sw.ElapsedMilliseconds);
			swritter.WriteLine("Czas remove podwojnego: \t" + sw.ElapsedMilliseconds);
			foreach(var w in T)
			{
				int i=0;
				Tpodw[i]=T[i++];
			}
			sw.Reset();
			
			// sprawdzamy remove słownika
			sw.Start();
			for (int i = 0; i < wielkoscProbyDoFora/100; i++)
			{
				int licznik=0;
				string slowo = generator(dlugoscStringa);
				foreach (KeyValuePair<int,string> s in slownik)
				{
					if (s.Key==licznik && s.Value==slowo)
						break;
					else licznik++;
				}
				
				slownik.Remove(licznik);
			}
			Console.WriteLine("Czas remove słownika: \t\t" + sw.ElapsedMilliseconds);
			swritter.WriteLine("Czas remove słownika: \t\t" + sw.ElapsedMilliseconds);
			sw.Reset();
		}
	}
}
